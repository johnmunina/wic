from pathlib import Path

import pytest
from wic import submission

data_dir: Path = Path(__file__).parent / "data"


def test_grading_justification_should_be_present():
    with pytest.raises(ValueError):
        submission.Submission.from_file(data_dir / "missing_justification.txt")


def test_grading_url_should_be_present():
    with pytest.raises(ValueError):
        submission.Submission.from_file(data_dir / "empty_file.txt")


def test_valid_file_should_load():
    s = submission.Submission.from_file(data_dir / "valid_file.txt")
