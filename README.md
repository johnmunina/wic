# Will It Clone?

Will It Clone?, also called `wic` is a tool to check the required
format for URL submissions.

## Usage:
```
usage: wic [-h] [--file] url

positional arguments:
  url         URL of submission

optional arguments:
  -h, --help  show this help message and exit
  --file      validate against a file
```

### Examples:
```
(wic) chris@dijkstra [11:42:26 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % wic https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic
Clone succeeded!
(wic) chris@dijkstra [11:42:42 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> %
```

```
(wic) chris@dijkstra [11:42:42 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % wic https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic.git
Clone succeeded!
(wic) chris@dijkstra [11:42:45 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> %
```

```
(wic) chris@dijkstra [11:42:50 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % wic https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic.git 
Clone succeeded!
(wic) chris@dijkstra [11:43:29 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % 
```

```
(wic) chris@dijkstra [11:42:45 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % wic https://gitlab.com/cwpitts-byui/cse-210/student-tools/wicccccc
Clone failed! Error was:
Cmd('git') failed due to: exit code(128)
  cmdline: git clone -v https://gitlab.com/cwpitts-byui/cse-210/student-tools/wicccccc /tmp/tmpcobth_ks
  stderr: 'Cloning into '/tmp/tmpcobth_ks'...
remote: 
remote: ========================================================================
remote: 
remote: The project you were looking for could not be found or you don't have permission to view it.
remote: 
remote: ========================================================================
remote: 
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
'
(wic) chris@dijkstra [11:42:50 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> %
```

```
(wic) chris@dijkstra [11:44:03 PM] [~/work/byui/cse-210/student-tools/wic] [master * *]
-> % cat scratch/foo.txt                                                      
git@gitlab.com:cwpitts-byui/cse-210/student-tools/wic.git
(wic) chris@dijkstra [11:44:07 PM] [~/work/byui/cse-210/student-tools/wic] [master * *]
-> % wic --file scratch/foo.txt                                       
Clone succeeded!
(wic) chris@dijkstra [11:44:11 PM] [~/work/byui/cse-210/student-tools/wic] [master * *]
-> % 
```

```
(wic) chris@dijkstra [11:44:11 PM] [~/work/byui/cse-210/student-tools/wic] [master * *]
-> % cat scratch/foo.txt
git@gitlab.com:cwpitts-byui/cse-210/student-tools/wicz
(wic) chris@dijkstra [11:44:31 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % wic --file scratch/foo.txt
Clone failed! Error was:
Cmd('git') failed due to: exit code(128)
  cmdline: git clone -v git@gitlab.com:cwpitts-byui/cse-210/student-tools/wicz /tmp/tmpqnphkkh3
  stderr: 'Cloning into '/tmp/tmpqnphkkh3'...
remote: 
remote: ========================================================================
remote: 
remote: The project you were looking for could not be found or you don't have permission to view it.
remote: 
remote: ========================================================================
remote: 
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
'
(wic) chris@dijkstra [11:44:34 PM] [~/work/byui/cse-210/student-tools/wic] [master *% *]
-> % 
```
