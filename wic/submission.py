from pathlib import Path


class Submission:
    def __init__(self, remote: str, justification: str):
        """
        Args:
          remote (str): Git remote URL as string
          justification (str): Grading justification
        """
        self.remote = remote
        self.justification = justification

    @classmethod
    def from_file(cls, path: Path):
        """ Load submission from file

        Args:
          path (Path): Path to file
        """
        with open(path, "r") as in_file:
            url: str = in_file.readline().strip()
            if not url:
                raise ValueError("Missing URL")

            justification: str = in_file.readline().strip()
            if not justification:
                raise ValueError("Missing grading justification")

        return cls(url, justification)

    def __repr__(self) -> str:
        """
        Returns:
          str: String representation of instance
        """
        return f"{self.remote}\n{self.justification[:20]}..."
