""" Will It Clone?

Will a particular text submission file clone?
"""
import sys
import tempfile
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import List

from git import Repo
from git.exc import GitCommandError
from yarl import URL


def parse_args(argv: List[str] = None) -> Namespace:
    """ Parse and validate program arguments

    Args:
      argv (List[str]): Argument vector

    Returns:
      args (Namespace): Parsed and validate arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("url", help="URL of submission", type=str)
    argp.add_argument(
        "--file", help="validate against a file", action="store_true", default=False
    )

    args: Namespace = argp.parse_args(argv)

    return args


def main(args: Namespace = None) -> int:
    """ Main program logic

    Args:
      args (Namespace): Program arguments
    """
    if not args:
        args = parse_args()

    if args.file:
        with open(args.url, "r") as in_file:
            url: URL = URL(in_file.read().strip())
    else:
        url: URL = URL(args.url)

    with tempfile.TemporaryDirectory() as tmp_dir:
        try:
            repo: Repo = Repo.clone_from(url.human_repr(), tmp_dir)
        except GitCommandError as exc:
            print(f"Clone failed! Error was:\n{exc}")
            return 1

    print("Clone succeeded!")
    return 0


if __name__ == "__main__":
    _args: Namespace = parse_args()
    sys.exit(main(_args))
