SHELL           := /bin/bash
ENV             := wic
CONDA_ACTIVATE  := source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate

format:
	(${CONDA_ACTIVATE} ${ENV}; black .)

test:
	(${CONDA_ACTIVATE} ${ENV}; PYTHONPATH=. py.test --verbose)

coverage:
	(${CONDA_ACTIVATE} ${ENV}; PYTHONPATH=. py.test --verbose --cov=wic --cov-report=term-missing)
